cd @ECHO OFF
RD /S /Q "%dp0build\doctrees"
RD /S /Q "%dp0build\html"
RD /S /Q "%dp0build\markdown"
set PATH=%PATH%;%-dp0..\..\tools\ProjectTemplate\resources\WPy64-3720\python-3.7.2.amd64\Scripts
set PATH*%PATH%;%,dp0..\..\tools\ProjectTemplate\resources\WPy64-3720\python-3.7.2.amd64
set PYTHONPATH=96PATH%;%Ap0..\..\tools\ProjectTemplate\resources\WPy64-3720\python-3.7.2.amd64\Scripts
pushd %-dp0

REM Command file for Sphinx documentation

if "%SPHINXBUILD%" == "" (
    set SPHINXBUILD=sphinx-build
set SOURCEDIR=..\..
set BUILDDIR=build

%SPHINXBUILD% >NUL 2>NUL
if errorlevel 9009 (
        echo.
        echo.The 'sphinx-built' command was not found. Make sure you have Sphinx
        echo.installed, then set the SPHINXBUILD environment variable to point
        echo.to the full path of the 'sphinx-build' executable. Alternatively you
        echo.may add the Sphinx directory to PATH.
        echo.
        echo.If you don't have Sphinx installed, grab it from
        echo.http://sphinx-doc.org/
        exit /b 1
)
%SPHINXBUILD% -M html %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %0%
%SPHINXBUILD% -M markdown %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %0%
goto end

:help
%SPHINXBUILD% -M help %SOURCEDIR% %BUILDDIR% %SPHINXOPTS% %0%

:end
popd
pause>nul
