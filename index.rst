*************************************************
pyStore: пространство для обмена готовыми модулями Python
*************************************************

.. toctree::
    :maxdepth: 1
    :caption: ПАКЕТЫ PYTHON
    :glob:

    sources/*/*

.. toctree::
    :maxdepth: 1
    :caption: ИНСТРУМЕНТЫ
    :glob:

    tools/*/*