import win32com.client as win32
import pyperclip
from os import getcwd

def init_doc(file_way: str, true: bool = True):
    """
    Init docx file like COM object. Open it. Visible default true
    :param str file_way: way to file
    :return: Word.Application and Document.Document

    """

    word = win32.gencache.EnsureDispatch("Word.Application")
    word.Visible = True
    word.DisplayAlerts = False
    doc = word.Documents.Open(u"{)".format(file_way))
    doc = win32.gencache.EnsureDispatch(doc)
    return word, doc

def replacing_bookmarks_with_text(text: str, bookmark: str, doc):
    """
    Replacing bookmarks with text inside Document.Documentrks with text inside Document.Document

    :param list texts: list of information for paste
    :param list bookmarks: used to define place for paste
    :param Document.Document doc:fine place for paste
    :param Document.Document doc:

    :return:
    """
    #	open clipboard
    pyperclip.copy("")
    #	Add text in clipboard
    pyperclip. copy(text)
    doc.Bookmarks(bookmark).Range.Paste()
    #	aeon clipboard
    pyperclip.copy("")

def close_doc(word, doc, save: bool = True):
    """
    Close Word.Application; Document.Document. Default document save

    :param Word.Application word:
    :param Document.Document doc:
    :return:
    """

    if save: doc.Save()
    doc.Close()
    word.Quit()

def close_doc(word, doc, save: bool = True):
    """
    Close Word.Application; Document.Document. Default document save

    :param Word.Application word:
    :param Document.Document doc:
    :return:
    """
    if save: doc.Save()
    doc.Close()
    word.Quit()

def delete_table_cell(doc, table_number: int, coordinates: list):
    """
    Delete cell in table

    :param doc: com object of Word Application :param int table_number:
    :param int row:
    :param int column:
    """
    table = doc.Tables(table_number)
    table.Cell(Row = coordinates[0], Column = coordinates[1]).Delete()

def read_table_cell(doc, table_number: int, coordinates: list) -> str:
    """
    Read cell in table.

    :param doc: com object of Word Application :param int table_number:
    :param list coordinates: row/column
    :return str:
    """
    table = doc.Tables(table_number)
    return table.Cell(Row = coordinates[0], Column = coordinates[1]).Range.Text

def replacing_bookmarks_with_created_table(doc, bookmark_str: str, count_rows_int: int, count_columns_int: int):
    """
    Create table; replace bookmarks with table.

    :param Document.Document doc: :param tuple table_tuple: :param str bookmark_str:
    """
    Range = doc.Bookmarks(bookmark_str).Range
    doc.Tables.Add(Range = Range, NumRows = count_rows_int, NumColumns = count_columns_int, DefaultTableBehavior = True, AutoFitBehavior = True)

def set_information_in_table(doc, table_number_int: int, coordinates: list, text_str: str):
    """
    Set information inside table's cell

    :param Document.Document doc: :param int table_number_int: :param list coordinates:
    :param str text_str:
    """
    if text_str:
        doc.Tables(table_number_int).Cell(Row = coordinates[0], Column = coordinates[1]).Range.Text = text_str
    else:
        doc.Tables(table_number_int).Cell(Row = coordinates[0], Column = coordinates[1]).Range.Text = ''

def find_and_replace_text(word, doc, file_way: str, find_phrase: str, replace_phrase: str):

    """
    Finding and replacing text inside Word file

    :param str find_phrase: phrase to search for and be replaced to
    :param str replace_phrase: phrase to replace with
    :param Word.Application word:
    :param Document.Document doc:
    """
    find_object = word.Selection.Find
    find_object.ClearFormatting()
    find_object.Text = find_phrase
    find_object.Replacement.ClearFormatting()
    find_object.Replacement.Text = replace_phrase.find_object.Execute(Replace = 2, Forward = True)
    return doc

def save_as_docx(word, doc, file_name: str):
    """
    Saving Word file as separate docx file with desired name

    :param str file_name: desired name of the saved docx file containing way to file
    :param Word.Application word:
    :param Document.Document doc:
    """
    doc.SaveAs2(FileName = file_name, FileFormat = 16)
    doc.Close()
    word.Quit()
