import win32com.client
import win32com.client as win32
import datetime # Datetine functions
import time
import pythoncom # from openpyxt import Goad workbook # function for work with defected Asx ( FilterDatobase)

def init_excel():
    """
    Init the excel com instance

    :return excel: com object of excel
    """
    # Init the excel instance retry_count_max_int = 10 retry_count_int = 0 while retry_count_int<retry_count_max_int: retry_count_int=retry_count_int+1
    try:
        pythoncom.CoInitialize()
        time.sleep(3)
        excel = win32com.client.gencache.EnsureDispatch('Excel.Application')
    except Exception as e:
        pass
    else:
        print(f"ONLY PRINT: Excel construct error (maybe after the dot net or Ax update on the VM). Retry # {retry_count_int}")
        pythoncom.CoUninitialize()
        time.sleep(4)
        excel.Visible = True
        excel.DisplayAlerts = False # excel—ScreenUpdating = Ease return excel

def destruct_excel(excel):
    """
    Delete the excel instance
    :param excel: com object of excel"""
    excel.DisplayAlerts = True
    excel.Quit()
    excel = None
    pythoncom.CoUninitialize()

def open_workbook(excel, file_path: str):
    """
    Open the workbook,
    :param excel: com object of excel
    :param str file_path: excel.AskToUpdateLinks = False """

    try:
        xlwb = excel.Workbooks(file_path)
    except Exception as e:
        try: xlwb = xlwb = excel.Workbooks.Open(file_path)
        except Exception as e:
            excel.AskToUpdateLinks = True
            raise e
    excel.AskToUpdateLinks =True
    return xlwb

def get_count_worksheet(workbook, file_path) -> int:
    """
    Get worksheet count. Index from 1
    :param workbook: com object of excel return workbook.Worksheets.Count def save_worksheet(workbook, file_path: str): Save excel workbook
    :param str file_path: path to saving file """
    workbook.SaveAs(Filename = file_path)

def close_workbook(workbook, savebool = True):
    """
    Save the workbook

    :param workbook: com object of excel :param bool ssavebool: default True"""
    workbook.Close(savebool)


def read_range2(workbook, worksheet_index_int: int, coordinates: list) -> tuple:
    """
    Read range from XIS

    :param workbook: com object of excel
    :param int worksheet_index_int: index of sheet (start by 1)
    :param list coordinates: list of coordinates
    :return tuple result: turple of turples of the cell value - None if cell is empty """

    result = None
    sheet = workbook.Worksheets(worksheet_index_int)
    result = sheet.Range( sheet.Cells(coordinates[0], coordinates[1]), sheet.Cells(coordinates[2], coordinates[3]) ).Value
    return result

def read_range(workbook, worksheet_index_int: int, from_row_int: int, from_col_int: int, to_row_int: int, to_col_int: int) -> tuple:
    """
    Read range from XLS

    :param workbook: com object of excel
    :param int worksheet_index_int: index of sheet (start by 1)
    :param int from_row_int: :param int from_col_int:
    :param int to_row_int: :param int to_col_int:
    :return tuple result: turple of turples of the cell value - None if cell is empty"""

    result = None
    sheet = workbook.Worksheets(worksheet_index_int)
    result = sheet.Range(sheet.Cells(from_row_int, from_col_int), sheet.Cells(to_row_int, to_col_int)).Value
    return result

def set_range(workbook, worksheet_index_int: int, from_row_int, from_col_int, data_list: list):
    """
    Set value in range

    :param workbook: com object of excel
    :param int worksheet_index_int: index of sheet (start by 1)
    :param int from_row_int:
    :param int from_col_int:
    :param list data_list: """
    sheet = workbook.Worksheets(worksheet_index_int)
    sheet.Range(sheet.Cells(from_row_int, from_col_int), sheet.Cells(from_row_int, from_col_int)).Value = data_list


def convert_excel_datetime_to_python_datetime(excel_datetime):
    """
    Convert excel datetime to python datetime

    :param excel_datetime:
    :return python_datetime:"""

    python_datetime = None
    try:
        python_datetime = datetime.datetime(year = excel_datetime.year, month = excel_datetime.month, day = excel_datetime.day,
                                            hour = excel_datetime.hour, minute = excel_datetime.minute, second = excel_datetime.second)
    except Exception as e:
        pass
    return python_datetime

def convert_excel_date_to_python_date(excel_date):
    """
    Convert excel date to python date

    :param excel_date:
    :return python_date: """
    python_date = None
    try:
        python_date = datetime.date(year = excel_date.year, month = excel_date.month, day = excel_date.day)
    except Exception as e:
        pass
    return python_date
# def remove error_fitter database(fiLe_path: str): • """ • Remove some bad Excel. error • try: wb = toad workbook(fitename = fiLe_path) wb.save(file path) • except Exception as e: pass

def read_checkbox(workbook, worksheet_index_int: int, checkbox_name: str) -> bool:
    """
    Read value checkbox

    :param workbook: com object of excel
    :param int worksheet_index_int: index of sheet (start by 1)
    :param str checkbox_name:
    :return bool:"""

    sheet = workbook.Worksheets(worksheet_index_int)
    shape_type = sheet.Shapes(checkbox_name).Type
    if shape_type == 8:
        return True if str(sheet.Shapes(checkbox_name).ControlFormat.Value) == '1.0' else False
    if shape_type == 12:
        return True if sheet.Shapes(checkbox_name).OLEFormat.Object.Object() == True else False

def set_checkbox(workbook, worksheet_index_int: int, checkbox_name: str, value: bool):
    """
    Set value in checkbox (type 8/12)

    :param workbook: com object of excel
    :param int worksheet_index_int: index of sheet (start by 1)
    :param str sheckbox_name:
    :param bool value: """

    sheet = workbook.Worksheets(worksheet_index_int)
    shape_type = sheet.Shapes(checkbox_name).Type
    if shape_type == 8: sheet.Shapes(checkbox_name).ControlFormat.Value = value
    if shape_type == 12: sheet.Shapes(checkbox_name).OLEFormat.Object.Object.Value = value

def read_combobox(workbook, worksheet_index_int: int, combobox_name: str) -> list:
    """
    Read value combobox

    :param workbook: com object of excel :param int worksheet_index_int: index of sheet (start by 1) :param str combobox_name: :return list:
    """

    sheet = workbook.Worksheets(worksheet_index_int)
    shape_type = sheet.Shapes(combobox_name).Type
    if shape_type == 12: return sheet.Shapes(combobox_name).OLEFormat.Object.Object.value


def set_combobox(workbook, worksheet_index_int: int, combobox_name: str, value: str):
    """
    Read value combobox

    :param workbook: com object of excel
    :param int worksheet_index_int: index of sheet (start by 1)
    :param str combobox_name:
    :param str value:
    :return list: """

    sheet = workbook.Worksheets(worksheet_index_int)
    shape_type = sheet.Shapes(combobox_name).Type
    if shape_type == 12: sheet.Shapes(combobox_name).OLEFormat.Object.Object.value = value

def read_option_button(workbook, worksheet_index_int: int, option_button_name_str: str) -> bool:
    """
    Read value option button

    :param workbook: com object excel :param int workcheet_index_int: index of sheet (start by 1) :param str option_button_name_str: :return bool•
    """
    sheet = workbook.WOrksheets(worksheet_index_int)
    shape_type = sheet.Shapes(option_button_name_str).Type
    if shape_type == 8:
        return True if str(sheet.Shapes(option_button_name_str).ControlFormat.Value) == '1.0' else False

def set_option_button(workbook, worksheet_index_int: int, option_button_name_str: str, value: bool):
    """
    Set value option button

    :param workbook: com object excel
    :param int worksheet_index_int: index of sheet (start by 1)
    :param str option_button_name_str:
    :param bool value:
    :return bool• """

    sheet = workbook.Worksheets(worksheet_index_int)
    shape_type = sheet.Shapes(option_button_name_str).Type
    if shape_type == 8: sheet.Shapes(option_button_name_str).ControlFormat.Value = value

def file_format_change(filepath, filepath2):
    """
    File format change

    :param filepath2: path to the file.
    :param file_path: path to save.
    :return: """

    excel = win32.gencache.EnsureDispatch('Excel.Application')
    wb2 = excel.Workbooks.Open(filepath2)
    wb2.SaveAs(filepath, FileFormat=51)
    wb2.Close()
    excel.Application.Quit()

def choose_value_drop_down(workbook, worksheet_index_int: int, drop_down_str: str, index_float: float):
    """
    Choose value depends on index of value.

    :param workbook: com object excel
    :param int workcheet_index_int: index of sheet (start by 1)
    :param str drop_down_str: name of Drop Down object
    :param float index_float: index of value in Drop Down. Start by 1.0 """

    sheet = workbook.Worksheets(worksheet_index_int)
    shape_type = sheet.Shapes(drop_down_str).Type
    if shape_type == 8: sheet.Shapes(drop_down_str).ControlFormat.Value = index_float

def count_rows(workbook, worksheet_index_int: int):
    """
    Count number of non-empty rows in a column

    :param workbook: com object excel
    :param int worksheet_index_int: index of sheet (start by 1)
    :return int: """

    sheet = workbook.Worksheets(worksheet_index_int)
    used = sheet.UsedRange
    nrows = used.Row + used.Rows.Count - 1
    return nrows

def count_columns(workbook, worksheet_index_int: int):
    """
    Count number of non-empty columns in a worksheet

    :param workbook: com object excel
    :param int worksheet_index_int: index of sheet (start by 1)
    :return int:
    """
    sheet = workbook. Worksheets(worksheet_index_int)
    used = sheet.UsedRange
    ncols = used.Column + used.Columns.Count - 1
    return ncols

def insert_to_cell(workbook, worksheet_index_int: int, row_num: int, col_num: int, paste_text: str):
    """
    Insert data in a particular cell in a worksheet

    :param workbook: com object excel
    :param int worksheet_index_int: index of sheet (start by 1)
    """
    sheet = workbook.Worksheets(worksheet_index_int)
    sheet.Cells(row_num, col_num).Value = paste_text
