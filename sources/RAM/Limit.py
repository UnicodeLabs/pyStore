import sys
import os
import signal
import time
import psutil
import logging
import threading

def ControlVirtualMemory(Logger: logging.Logger = None, LimitPersentFloat: float = 95.0, LimitMbfloat: float = 4000.0, ProgramNameStr: str = "Robot", TimeSleepInt: int = 5) -> None:
    """
    Controling virtual memory. Close program if it use
    more than LimitPersentFloat (default = 95.0) of virtual memory and also send message in Outlook if it needs.

    :param logging.Logger Logger:
    :param float LimitPersentFloat: default = 90.0
    :param str ProgramNameStr: default = "Robot"
    :param int TimeSleepint: default = 5
    :param bool SendMessage: default = False
    :param str SMTPHostStr: default = None
    :param int SMTPPortInt: default = None
    :param str LoginStr: default = None
    :param str PasswordStr: default = None
    :param list ToList: default = None
    :param list CCList: default = None
    :param str SubjectStr: default = None
    :param str MessageStr: default = None
    :param list AttachmentPathList: default = None
    """

    def Control() -> None:
        while True:
            # Get virtual memory in percent
            PersentVirtualMemoryFloat = psutil.virtual_memory().percent
            # Check limit of percent
            if PersentVirtualMemoryFloat > LimitPersentFloat:
            # Write log about situation
            Logger.error(f'Used virtual memory is (PersentVirtualMemoryFloat)%. I close {ProgramNameStr}')
            #	Close program os.kill(os.getpid(), signal.SIGTERM)
            #	exit()
            VirtualMemoryStorage = psutil.Process(os.getpid()).memory_full_info().rss
            VMSMb = VirtualMemoryStorage / 1024 / 1024
            #	Check Limit of Mb if VMSMb > LimitMbFloat:
            #	Check necessity to send message in Outlook
            #	Write tog about situation
            Logger.error(f'Used virtual memory is (VMSMb). I close {ProgramNameStr}')
            #	Close program os.kill(os.getpid(), signal.SIGTERM)
            #	exit()
            #	Time to sleep
            time.sleep(TimeSleepInt)  # Starts Control() in flow
    threading.Thread(target=Control).start()
