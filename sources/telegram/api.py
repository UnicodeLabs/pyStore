import requests
import time

def post_exists(in_channel_int, in_post_int):
    l_post_response = requests.get(f"https://t.me/{in_channel_int}/{in_post_int}?embed=1")
    return "tgme_widget_message_error" not in l_post_response.text

def post_last_get(in_channel_str):
    """Последний опубликованный пост на канале
    
    """
    l_right_step_int = 100
    l_left_step_int = 10
    l_request_interval_float = 0.4
    l_post_int=l_right_step_int
    # ШАГИ ВПРАВО ДО ПЕРВОГО НЕОПУБЛИКОВАННОГО ШАГ 100
    while post_exists(in_channel_int = in_channel_str, in_post_int=l_post_int) == True:
        l_post_int += l_right_step_int
        time.sleep(l_request_interval_float)
    l_post_int-=l_left_step_int
    # ШАГИ ВЛЕВО ДО ПЕРВОГО ОПУБЛИКОВАННОГО ШАГ 10
    while post_exists(in_channel_int = in_channel_str, in_post_int=l_post_int) == False:
        l_post_int-=l_left_step_int
        time.sleep(l_request_interval_float)
    # ШАГИ ВПРАВО ДО ПЕРВОГО ОПУБЛИКОВАННОГО ШАГ 1
    while post_exists(in_channel_int = in_channel_str, in_post_int=l_post_int) == True:
        l_post_int+=1
        time.sleep(l_request_interval_float)
    return l_post_int-1