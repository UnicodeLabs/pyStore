from logging import StreamHandler
import threading
import datetime
import socket
import getpass
import psycopg2

class Handler(StreamHandler):
    """
    HOW TO USE
    from LOGS import LOGS_API_v1
    1LoggingHandler = LOGS_API_vl.Handler(
    inNHostStr="localhost", inPGPortInt.5432, inPGUserStr="pgadmin", inPGPasswordStr=1PGPassStr, inRobotStr="test_name", inAdditionalStr."-")
    '4 add handler to logger object
    mRobotLogger.addHandler(lLoggingHandler)
    """

    def init (self, inPGHostStr, inPGPortInt, inPGUserStr, inPGPasswordStr, inRobotStr, inAdditionalStr):
        StreamHandler.init(self)
        #	Detect host name t user name (aLt in upper case)
        self.mRobotHostUpperStr = socket.gethostnamea.upper()
        self.mRobotUserUpperStr = getpass.getuser().upper()
        #	Assign
        self.mPGHostStr = inPGHostStr
        self.mPGPortInt = inPGPortInt
        self.mPGUserStr = inPGUserStr
        self.mPGPasswordStr = inPGPasswordStr
        self.mPGDatabaseStr = "Logs"
        if inRobotStr is None: inRobotStr=""
        if inAdditionalStr is None: inAdditionalStr=""
        self.mRobotStr = inRobotStr.upper()
        self.mAdditionalStr = inAdditionalStr
        self.mPGDBConnection = None
        #	EstabLish the connection
        self.__PG_Establish__()

def PG_Establish (self):
    """
    :param self:
    :return:
    """
    try:
        # Generate Application name from Robot name: Additional.
        lApplicationNameStr = f"{self.mRobotStr}:{self.mAdditionalStr}"
        self.mPGDBConnection = psycopg2.connect(
        host=self.mPGHostStr, port=self.mPGPortInt, user=self.mPGUserStr, password = self.mPGPasswordStr,
        database = self. mPGDatabaseStr, application_name=lApplicationNameStr)
    except Exception as e:
        print(f"Exception when init the connection to Postgres LOGS BD. See exception: (str(e))")

def PG_Send (self, inRecord):
    """

    :param self:
    :param inRecord:
    :return:
    """

    lRecordDatetime = datetime.datetime.now()
    lPGEstablishRetryCountInt = 2
    for liter in list(range(lPGEstablishRetryCountInt)):
        try:
            lSQLStr = f'INSERT INTO "Raw" ("Datetime, "Host", "User', 'Robot", "Additional", "Level", "Message") VALUES(Xs, Xs, Xs, Xs, Xs, %s, %s);'
            lRecordHostStr = self.mRobotHostUpperStr
            lRecordUserStr = self.mRobotUserUpperStr
            lRecordRobotStr = self.mRobotStr
            lRecordAdditionalStr = self.mAdditionalStr
            lRecordLevelStr = inRecord.levelname
            lRecordMessageStr = inRecord.msg
            lPGCursor = self.mPGDBConnection.cursor()
            lPGCursor.execute(lSQLStr, (
                lRecordDatetime,
                lRecordHostStr,
                lRecordUserStr,
                lRecordRobotStr,
                lRecordAdditionalStr,
                lRecordLevelStr,
                lRecordMessageStr)
            )
            self.mPGOBConnection.commit() # Commit the SQL
            lPGCursor.close()
            break
        except Exception as e:
            print(f"Try to reestablish connection to DB. See exception from execution: (str(e))")
            self.PG_Establish()

def close(self):
    self.mPGDBConnection.close()

def emit(self, inRecord):
    """
    A !nit in new thread
    """
    threading.Thread(target=self.PG_Send, args=[inRecord]).start()
