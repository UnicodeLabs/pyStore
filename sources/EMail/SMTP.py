import smtplib
import logging
from os import path
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

#	Test - send emaiL
#	UtitsEmoiL.Send(inLoggerInstonce = (Jogger, inSMTPHOstStr = inGSettings("EMair]["HostStr"], inSMTPPortInt = inGSettings("EMoiL"]("PortInt"],
#	inLoginStr = inGSettings("EMbit"]("LoginStr"], inPasswordStr = inGSettings("EMOiLW"PosswordStr"],
#	inToList = plMosLovpIsk.ru"], inCCList = (1,
#	inSubjectStr = "Test", inMessageStr = "Test message 1n# # # # # # # # # # # # # VIC yeawewuem, 1nPo6om knknaff LET WIT", inAttachmentPathList = [
#	r"C:VAIDocskExampLe.xLsx",
#	r"C:VIDocsknet foLder.txt"
#	]

def Send(inLoggerInstance, inSMTPHostStr, inSMTPPortInt, inLoginStr, inPasswordStr, inToList, inCCList, inSubjectStr, inMessageStr, inAttachmentPathList):

    """
    Send EMaiL with attachment

    :param logger inLoggerinstance:
    :param str inSMTPHostStr:
    :param int inSMIPPortInt:
    :param str inLoginStr:
    :param str inPasswordStr:
    :param list inToList:
    :param list inCCList:
    :param str inSubjectStr:
    :param str inMessageStr:
    :param list inAttachmentPathList:
    """
    try:
        #	Отправка писем
        lMIMEContainer = MIMEMultipart()
        lMIMEContainer['Subject'] = inSubjectStr # Set the subject
        lMIMEContainer['From'] = inLoginStr # Set the from email
        lMIMEContainer['To'] = "; ".join(inToList) # Create List of to emaiLs ' \
        lMIMEContainer['CC'] = "; ".join(inCCList) # Create List of to cc emaits
        #	Fitl the attachment file List
        for lAttachmentPathItem in inAttachmentPathList: # Loop
            lAttachmentNameStr = path.basename(lAttachmentPathItem)
            lAttachmentFile = open(lAttachmentPathItem, "rb")
            lMIMEFilePart = MIMEBase('application', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            lMIMEFilePart.set_payload((lAttachmentFile).read())
            encoders.encode_base64(lMIMEFilePart)
            lMIMEFilePart.add_header('Content-Disposition',"attachment", filename=lAttachmentNameStr)
            lMIMEContainer.attach(lMIMEFilePart)
            lAttachmentFile.close()
        lMIMEContainer.attach(MIMEText(inMessageStr, 'plain')) # Attach message body plain text
        lSMTPInstance = smtplib.SMTP(host=inSMTPHostStr, port=str(inSMTPPortInt)) # Create connection 1SMTPInstance.starttls() # Start TSL cryptography
        lSMTPInstance.login(inLoginStr, inPasswordStr) # Login Credentials for sending the mail # send the message via the server.
        lDestinationEMailList = '; '.join(inToList+inCCList)
        if ';' in lDestinationEMailList:
            lSMTPInstance.sendmail(lMIMEContainer['From'], inToList+inCCList, lMIMEContainer.as_string())
        else:
            lSMTPInstance.sendmail(lMIMEContainer['From'], lDestinationEMailList, lMIMEContainer.as_string())
        lSMTPInstance.quit() # Logoff from smtp
    except Exception as e:
        if inLoggerInstance: inLoggerInstance.exception(f"Возникла ошибка при отправке сообщения. To: (inToList), "
                f"Subject: {inSubjectStr}, Message: {inMessageStr}, AttachmentsPathList: {inAttachmentPathList}"
        else: print(e)
