import smtplib
import logging
from os import path
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.message import EmailMessage
from email.utils import make_msgid
from email import encoders
import mimetypes
import jinja2
import traceback

#	Test - smtp_send emaiL
#	EMail_API_v1.smtp_send(inLoggerInstonce = (Jogger, g_smtp_host_str = inGSettings("EMair]["HostStr"], g_smtp_port_int = inGSettings("EMoiL"]("PortInt"],
#	in_from_email_str = inGSettings("EMbit"]("LoginStr"], in_password_str = inGSettings("EMOiLW"PosswordStr"],
#	in_to_list = plMosLovpIsk.ru"], in_cc_list = (1,
#	in_subject_str = "Test", in_message_str = "Test message 1n# # # # # # # # # # # # # VIC yeawewuem, 1nPo6om knknaff LET WIT", in_attachment_path_list = [
#	r"C:VAIDocskExampLe.xLsx",
#	r"C:VIDocsknet foLder.txt"
#	]

g_smtp_host_str = "smtp.yandex.ru"
g_smtp_port_str = "587"


def smtp_send(in_from_email_str, in_password_str, in_to_list,  in_subject_str, in_message_str, in_cc_list=None, in_attachment_path_list=None, in_logger=None):

    """
    smtp_send EMaiL with attachment

    :param logger in_logger:
    :param str in_from_email_str:
    :param str in_password_str:
    :param list in_to_list:
    :param list in_cc_list:
    :param str in_subject_str:
    :param str in_message_str:
    :param list in_attachment_path_list:
    """
    try:
        global g_smtp_port_str
        global g_smtp_host_str
        if in_cc_list is None: in_cc_list = []
        if in_attachment_path_list is None: in_attachment_path_list = []
        #	Отправка писем
        l_mime_container = MIMEMultipart()
        l_mime_container['Subject'] = in_subject_str # Set the subject
        l_mime_container['From'] = in_from_email_str # Set the from email
        l_mime_container['To'] = "; ".join(in_to_list) # Create List of to emaiLs ' \
        l_mime_container['CC'] = "; ".join(in_cc_list) # Create List of to cc emaits
        #	Fitl the attachment file List
        for l_attachment_path_item in in_attachment_path_list: # Loop
            l_attachment_name_str = path.basename(l_attachment_path_item)
            l_attachment_file = open(l_attachment_path_item, "rb")
            l_mime_file_part = MIMEBase('application', 'vnd.openxmlformats-officedocument.spreadsheetml.sheet')
            l_mime_file_part.set_payload((l_attachment_file).read())
            encoders.encode_base64(l_mime_file_part)
            l_mime_file_part.add_header('Content-Disposition',"attachment", filename=l_attachment_name_str)
            l_mime_container.attach(l_mime_file_part)
            l_attachment_file.close()
        l_mime_container.attach(MIMEText(in_message_str, 'plain')) # Attach message body plain text
        l_smtp = smtplib.SMTP(host=g_smtp_host_str, port=str(g_smtp_port_str)) # Create connection 1SMTPInstance.starttls() # Start TSL cryptography
        l_smtp.set_debuglevel(1) # Необязательно; так будут отображаться данные с сервера в консоли

        if in_password_str is not None:
            l_smtp.ehlo() # Кстати, зачем это? 
            l_smtp.starttls()
            l_smtp.login(in_from_email_str, in_password_str) # Login Credentials for smtp_sending the mail # smtp_send the message via the server.
        lDestinationEMailList = '; '.join(in_to_list+in_cc_list)
        if ';' in lDestinationEMailList:
            l_smtp.sendmail(l_mime_container['From'], in_to_list+in_cc_list, l_mime_container.as_string())
        else:
            l_smtp.sendmail(l_mime_container['From'], lDestinationEMailList, l_mime_container.as_string())
        l_smtp.quit() # Logoff from smtp
    except Exception as e:
        if in_logger: in_logger.exception(f"Возникла ошибка при отправке сообщения. To: {in_to_list}, Subject: {in_subject_str}, Message: {in_message_str}, AttachmentsPathList: {in_attachment_path_list}")
        else: print(e)



def smtp_html_send(in_from_name_str, in_from_email_str, in_password_str, in_to_list, in_cc_list, in_bcc_list, in_subject_str, in_plain_str, in_cid_image_dict, in_jinja_html_path_str, in_jinja_context_dict, in_attachment_path_list):

    """
    smtp_send HTML (jinja2 template with context dict) EMaiL with attachment
    Recommend to use

    from EMail import EMail_API_v1
    l_cid_image_dict= {"img_rpa": "img_rpa.jpg", "img_pyopenrpa": "img_pyopenrpa.jpg"}
    l_attachment_list=["img_rpa.jpg"]
    EMail_API_v1.smtp_html_send(in_from_email_str="RPA_01@pyopenrpa.ru", in_password_str=None, 
        in_to_list=["Maslov.Ivan@pyopenrpa.ru"], in_cc_list=[], in_subject_str="HEADER: Test HTML Letter", 
        in_jinja_html_path_str="LetterHTML.html", 
        in_jinja_context_dict={"BodyStr":"Hello my dear world!<li>Point1</li>", "RobotNameStr":"PER_Perezakaz"},
        in_cid_image_dict=l_cid_image_dict, in_attachment_path_list=l_attachment_list)

    :param logger in_logger:
    :param str in_from_email_str:
    :param str in_password_str:
    :param list in_to_list:
    :param list in_cc_list:
    :param str in_subject_str:
    :param str in_plain_text_str:
    :param dict in_cid_image_dict: {"image1": "path\\to\\image", "image2":...} in HTML: <img src="cid:image1">
    :param str in_jinja_html_path_str:
    :param str in_jinja_context_dict:
    :param list in_attachment_path_list:
    """

    global g_smtp_port_str
    global g_smtp_host_str
    #	Отправка писем
    l_mime_container = EmailMessage()
    l_mime_container['Subject'] = in_subject_str # Set the subject
    l_mime_container['From'] = f"{in_from_name_str} <{in_from_email_str}>" # Set the from email
    l_mime_container['To'] = ", ".join(in_to_list) # Create List of to emaiLs ' \
    l_mime_container['CC'] = ", ".join(in_cc_list) # Create List of to cc emaits
    l_mime_container['Bcc'] = ", ".join(in_bcc_list) # Create List of to cc emaits

    # set the plain text body
    l_mime_container.set_content(in_plain_str)

    # now create a Content-ID for the image
    #image_cid = make_msgid(domain='pyopenrpa.ru')
    #image_cid = "<olololo>"
    #in_jinja_context_dict.update({"image_cid":image_cid[1:-1]})

    #HTML Template
    l_system_loader_path_str = "/".join(in_jinja_html_path_str.split("\\")[0:-1])
    l_template_file_name_str = in_jinja_html_path_str.split("\\")[-1]
    l_refresh_html_jinja2_loader = jinja2.FileSystemLoader(l_system_loader_path_str)
    l_refresh_html_jinja2_env = jinja2.Environment(loader=l_refresh_html_jinja2_loader, trim_blocks=True)
    l_refresh_html_jinja2_template = l_refresh_html_jinja2_env.get_template(l_template_file_name_str)
    l_html_str = l_refresh_html_jinja2_template.render(**in_jinja_context_dict) # Render the template into str
    l_mime_container.add_alternative(l_html_str, subtype='html')

    # Loop for import mime images
    for l_cid_str in in_cid_image_dict:
        l_image_path = in_cid_image_dict[l_cid_str]
        fp = open(l_image_path, 'rb')
        # know the Content-Type of the image
        maintype, subtype = mimetypes.guess_type(fp.name)[0].split('/')
        # attach it
        l_mime_container.get_payload()[1].add_related(fp.read(), 
                                                maintype=maintype, 
                                                subtype=subtype, 
                                                cid=f"<{l_cid_str}>")
        fp.close()
    # Loop for the attachments
    for l_path_str in in_attachment_path_list:
        fp = open(l_path_str, 'rb')
        # know the Content-Type of the image
        maintype, subtype = mimetypes.guess_type(fp.name)[0].split('/')
        # attach it
        l_mime_container.add_attachment(fp.read(), maintype=maintype, subtype=subtype)
        fp.close()
    
    l_smtp = smtplib.SMTP(host=g_smtp_host_str, port=str(g_smtp_port_str)) # Create connection 1SMTPInstance.starttls() # Start TSL cryptography
    if in_password_str is not None:
        l_smtp.ehlo() # Кстати, зачем это? 
        l_smtp.starttls()
        l_smtp.login(in_from_email_str, in_password_str) # Login Credentials for smtp_sending the mail # smtp_send the message via the server.
    l_smtp.send_message(l_mime_container)
    l_smtp.quit() # Logoff from smtp

def exception_send(in_from_email_str, in_password_str, in_to_list, in_subject_str):
    l_message_str = f"ВНИМАНИЕ! Возникло неожидаемое исключение при выполнении робота. \n Обратите внимание, что результат робота может быть сформирован некорректно! \n\nОбратитесь в службу поддержки робота с этим сообщением: \n{traceback.format_exc()}"
    smtp_send(in_from_email_str=in_from_email_str,in_password_str=in_password_str,
        in_to_list=in_to_list, in_subject_str=in_subject_str, in_message_str=l_message_str)
    #smtp_html_send(in_from_email_str=in_from_email_str,in_password_str=in_password_str,
    #    in_to_list=in_to_list, in_subject_str=in_subject_str,
    #    in_jinja_html_path_str=, in_jinja_context_dict=,)